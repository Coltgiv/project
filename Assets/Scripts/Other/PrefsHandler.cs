﻿//Skrypt do obsługi prefów.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefsHandler : MonoBehaviour
{

    #region Singleton
    private static PrefsHandler _instance;                              
    public static PrefsHandler Instance { get { return _instance; } }
    #endregion
    #region Generic Methods
    //Zapewnienie jednego obiektu singletonu.
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;

        }
    }
    #endregion
    #region Non-Generic Methods
    //Ustawienie prefów intem.
    public void SetIntPref(string name, int value)
    {
        PlayerPrefs.SetInt(name, value);
    }
    //Pobranie z prefów wartości int.
    public int GetIntPref(string name)
    {
        return PlayerPrefs.GetInt(name);
    }
    //Usunięcie wszystkich prefów.
    public void DeleteAllPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
    //Uaktualnienie prefa odpowiedzialnego za podany parametr.
    public void SetParamPref(string name, int index)
    {
        int value = GetIntPref(name + index);
        value++;
        SetIntPref(name + index, value);
    }
    #endregion
}
