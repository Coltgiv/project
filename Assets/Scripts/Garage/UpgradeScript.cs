﻿//Skrypt odpowiedzialny za obsługę ulepszeń.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

public class UpgradeScript : MonoBehaviour
{
    #region Parameters
    private int[] prices;                   //Tablica przechowująca ceny.
    private Image[] bars = new Image[4];    //Tablica przechowująca obrazki(pasek ulepszenia).
    private TextMeshProUGUI buttonText;     //Napis na przycisku.
    private GameObject upgradeButton;       //Przycisk ulepszenia.
    private GameObject upgradeBar;          //Pasek ulepszenia.
    private int imageIndex = 0;             //Indeks obrazka.
    private int priceIndex = 0;             //Indeks ceny.
    private string name;              //Nazwa rodzica.
    private int carIndex;                   //Indeks pojazdu.
    #endregion
    #region Generic Methods
    // Przygotowanie do działania.
    void Start()
    {
        GetObjects();
        SetPrices();
        SetStartingPoint();
        SetTextOnButton();
    }
    //Jeżeli nas stać na ulepszenie, aktywujemy przycisk.
    private void Update()
    {
        if(priceIndex < 4)
        {
            if (prices[priceIndex-1] <= GarageUI.Instance.GetCoins())
            {
                ActivateButton();
            }
        }
    }
    #endregion
    #region Non-Generic Methods
    //Uaktualnienie ceny ulepszenia na przycisku, jeżeli nas nie stać to deaktywujemy przycisk.
    //W przypadku gry maksymalnie ulepszyliśmy na przycisku wyświetlany jest napisa "Full".
    private void SetTextOnButton()
    {
        if(priceIndex < 4)
        {
            buttonText.text = prices[priceIndex].ToString() + "$";
            priceIndex++;
            if (prices[priceIndex-1] > GarageUI.Instance.GetCoins())
            {
                DeactivateButton();
            }
        }
        else
        {
            buttonText.text = "Full";
        }
    }
    // Przypisanie obiektów paska ulepszeń, przycisku oraz napisu na przycisku.
    // Przypisanie nazwy rodzica oraz indeksu pojazdu.
    private void GetObjects()
    {
        upgradeButton = transform.Find("UpgradeButton").gameObject;
        buttonText = upgradeButton.GetComponentInChildren<TextMeshProUGUI>();
        upgradeBar = transform.Find("UpgradeBar").gameObject;
        int i = 0;
        foreach (Image image in upgradeBar.GetComponentsInChildren<Image>())
        {
            bars[i] = image;
            i++;
        }
        name = transform.parent.name;
        carIndex = GarageUI.Instance.carIndex;
    }
    //Po wciśnięciu przycisku uaktualniamy pasek, aktualizujemy liczbę monet, napis na przycisku oraz parametry.
    public void OnClick()
    {
        ChangeColor(bars[imageIndex]);
        imageIndex++;
        GarageUI.Instance.UseCoins(prices[priceIndex-1]);
        PrefsHandler.Instance.SetParamPref(name, carIndex);
        SetTextOnButton();
        if(imageIndex > 3)
        {
            DeactivateButton();
        }
    }
    //Ustawienie początkowego stanu na podstawie prefów.
    private void SetStartingPoint()
    {
        int startingPoint = PrefsHandler.Instance.GetIntPref(name + carIndex);
        for(int i = 0; i < startingPoint; i++)
        {
            ChangeColor(bars[imageIndex]);
            imageIndex++;
            SetTextOnButton();
        }

    }
    //Zmiana koloru na pasku.
    public void ChangeColor(Image image)
    {
        image.color = new Color32(39, 125, 16, 255);
    }
    //Deaktywacja przycisku.
    private void DeactivateButton()
    {
        transform.GetComponentInChildren<Button>().interactable = false;
        transform.GetComponentInChildren<Button>().GetComponentInChildren<TextMeshProUGUI>().color = new Color32(255, 0, 0, 255);
    }
    //Aktywacja przycisku.
    private void ActivateButton()
    {
        transform.GetComponentInChildren<Button>().interactable = true;
        transform.GetComponentInChildren<Button>().GetComponentInChildren<TextMeshProUGUI>().color = new Color32(255, 255, 255, 255);
    }
    //Ustawienie cen.
    public void SetPrices()
    {
        prices = new int[4];
        prices = GarageUI.Instance.GetPrices(name);
    }
    #endregion
}
