﻿//Obiekt przechowujący listę wszystkich pojazdów.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cars : ScriptableObject
{
    #region Parameters
    public Car[] cars;  //Lista dostępnych pojazdó.
    #endregion
    #region Generic Methods
    //Wczytanie pojazdów do listy.
    private void Awake()
    {
        cars = new Car[2];
        cars[0] = ScriptableObject.CreateInstance<Car1>();
        cars[1] = ScriptableObject.CreateInstance<Car2>();
    }
    #endregion
    #region Getters
    //Pobranie obiektu pojazdu na podstawie indeksu.
    public Car GetCar(int index)
    {
        return cars[index];
    }
    #endregion
}
