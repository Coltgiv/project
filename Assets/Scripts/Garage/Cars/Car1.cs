﻿//Skrypt pojazdu numer 1.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car1 : Car
{
    #region Generic Methods
    //Parametry pojazdu.
    public Car1()
    {
        this.enginePrices = new int[] { 75, 150, 300, 500 };
        this.tankPrices = new int[] { 75, 150, 300, 500 };
        this.tiresPrices = new int[] { 75, 150, 300, 500 };
        this.bumperPrices = new int[] { 75, 150, 300, 500 };
        this.gunPrices = new int[] { 75, 150, 300, 500 };
        this.engineParameters = new float[] { 500, 750, 100, 1250, 1500 };
        this.tankParameters = new int[] {200, 750, 1000, 1250, 1500 };
        this.tiresParameters = new float[] {5f, 7.5f, 10f, 12.5f, 15f };
        this.bumperParameters = new int[] { 5, 7, 9, 13, 15 };
        this.gunParameters = new int[] { 0, 5, 10, 15, 20 };
    }
    //Sprite pojazdu.
    private void OnEnable()
    {
        this.sprite = Resources.Load<Sprite>("Models/Cars/FirstCar");
    }
    #endregion
}
