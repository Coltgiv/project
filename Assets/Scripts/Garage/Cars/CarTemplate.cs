﻿//Obiekt który jest prototypem klas pojazdów.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : ScriptableObject
{
    #region Parameters
    public int price;               //Cena pojazdu.
    public int[] enginePrices;      //Ceny ulepszeń silnika.
    public int[] tankPrices;        //Ceny ulepszeń baku.
    public int[] tiresPrices;       //Ceny ulepszeń opon.
    public int[] bumperPrices;      //Ceny ulepszeń zderzaka.
    public int[] gunPrices;         //Ceny ulepszeń broni.
    public float[] engineParameters;//Parametry silnika.
    public int[] tankParameters;    //Parametry baku.
    public float[] tiresParameters; //Parametry opon.
    public int[] bumperParameters;  //Parametry zderzaka.
    public int[] gunParameters;     //Parametry broni.
    public Sprite sprite;           //Sprite pojazdu.
    #endregion
    #region Non-Generic Methods
    //Pobranie cen w zależności od podanej nazwy.
    public int[] GetPrice(string name)
    {
        if(name == "Engine")
        {
            return enginePrices;
        }else if(name == "Tank")
        {
            return tankPrices;
        }else if(name == "Tires")
        {
            return tiresPrices;
        }else if(name == "Bumper")
        {
            return bumperPrices;
        }
        else
        {
            return gunPrices;
        }
    }
    #endregion
}

