﻿//Skrypt odpowiedzialny za UI w trakcie gry.
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
public class InGameUI : MonoBehaviour
{
    #region Parameters
    public Slider fuel;                     //Pasek paliwa.
    public Slider hp;                       //Pasek życia.
    public PlayerScript player;              //Obiekt gracza.
    public TextMeshProUGUI numberOfCoins;   //Liczba monet.
    public TextMeshProUGUI deathMessage;    //Tekst wyświetlany po śmierci.
    private GameObject deathScreen;         //Obiekt wyświetlany po śmierci.
    private GameObject ammoObject;          //Obiekt związany z wyświetlaniem informacji o amunicji.
    public TextMeshProUGUI ammunition;      //Liczba amunicji.
    #endregion
    #region Generic Methods
    // Na początku ustawiamy wartości pasków życia, paliwa. Wyłączamy wyświetlanie ekranu końcowego oraz amunicji.
    void Start()
    {
        fuel.maxValue = player.GetMaxFuel();
        hp.maxValue = player.GetMaxHp();
        deathScreen = GameObject.Find("DeathScreen");
        deathScreen.SetActive(false);
        ammoObject = GameObject.Find("Ammo");
        ammoObject.SetActive(false);
    }
    //  Aktualizacja pasków życia i paliwa, aktualizacja liczby monet. Aktualizacja amunicji(jeżeli gracz posiadał amunicję).
    private void FixedUpdate()
    {
        fuel.value = player.GetFuel();
        hp.value = player.GetHp();
        numberOfCoins.text = player.GetCoins().ToString();
        if (player.GetAmmo() > 0)
        {
            ammoObject.SetActive(true);
        }
        ammunition.text = player.GetAmmo().ToString();
    }
    #endregion
    #region Non-Generic Methods
    //  Metoda odpowiedzialna za wyświetlanie  ekranu śmierci.
    public void ShowDeathScreen()
    {
        if (player.GetFuel() == 0)
        {
            deathMessage.text = "Your car ran out of fuel!";
        }
        else
        {
            deathMessage.text = "Your car is destroyed!";
        }
        deathScreen.SetActive(true);
    }
    //Powrót do garażu.
    public void ReturnToGarage()
    {
        PlayerPrefs.SetInt("Coins", player.GetCoins());
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    //Zakończenie aplikacji
    public void Exit()
    {
        Application.Quit();
    }
    #endregion
}