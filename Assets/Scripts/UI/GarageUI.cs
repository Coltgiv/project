﻿//Skrypt odpowiedzialny za działanie UI garażu.
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class GarageUI : MonoBehaviour
{
    #region Parameters
    private int coins;                      //Liczba monet.
    public TextMeshProUGUI numberOfCoins;   //Tekst wyświetlający liczbę monet.
    public int carIndex;                    //Indeks pojazdu.
    public Car carObject;                   //Obiekt pojazdu.
    private Cars cars;                      //Obiekt listy pojazdów.
    #endregion
    #region Singleton
    private static GarageUI _instance;
    public static GarageUI Instance { get { return _instance; } }
    #endregion
    #region Generic Methods
    //Zapewnienie singletonu, pobranie liczby monet i wyświetlenie jej, pobranie indeksu pojazdu raz przygotowanie paska.
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        coins = PrefsHandler.Instance.GetIntPref("Coins");
        numberOfCoins.text = coins.ToString();
        carIndex = PrefsHandler.Instance.GetIntPref("Index");
        PrepareBars();
    }
    private void Start()
    {
        //coins = PrefsHandler.Instance.GetIntPref("Coins");
        //numberOfCoins.text = coins.ToString();
        //carIndex = PrefsHandler.Instance.GetIntPref("Index");
        //PrepareBars();
    }
    //Aktualizacja liczby monet.
    private void Update()
    {
        numberOfCoins.text = coins.ToString();
    }
    #endregion
    #region Non-Generic Methods
    //Przygotowanie obiektu dla pasków.
    private void PrepareBars()
    {
        cars = ScriptableObject.CreateInstance<Cars>();
        carObject = cars.cars[carIndex];
    }
    //Rozpoczęcie rozgrywki.
    public void StartGame()
    {
        PrefsHandler.Instance.SetIntPref("Coins", coins);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }
    //Wyście z aplikacji.
    public void Exit()
    {
        PrefsHandler.Instance.SetIntPref("Coins", coins);
        Application.Quit();
    }
    #endregion
    #region Getters
    //Getter coins.
    public int GetCoins()
    {
        return coins;
    }
    //Zmniejszenie liczby monet o daną wartość.
    public void UseCoins(int price)
    {
        coins -= price;
    }
    //Pobranie cen.
    public int[] GetPrices(string name)
    {
        return carObject.GetPrice(name);
    }
    #endregion
}