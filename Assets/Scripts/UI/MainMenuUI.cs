﻿//Skrypt odpowiedzialny za działanie głownego menu.
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuUI : MonoBehaviour
{
    #region Parameters
    private GameObject continueButton;         //Obiekt wyświetlany po śmierci.
    #endregion
    #region Generic Methods
    //Jeżeli istnieją prefy to można kontynuować grę.
    private void Start()
    {
        continueButton = GameObject.Find("Continue");
        continueButton.SetActive(false);
        if (PlayerPrefs.HasKey("Coins") && (PlayerPrefs.GetInt("Coins") != 0))
        {
            continueButton.SetActive(true);
        }
    }
    #endregion
    #region Non-Generic Methods
    //   Rozpoczęcie rozgrywki. Usunięcie prefów, oraz wyzerowanie ulepszeń.
    public void StartGame()
    {
        PrefsHandler.Instance.DeleteAllPrefs();
        PrefsHandler.Instance.SetIntPref("CarIndex", 0);
        PrefsHandler.Instance.SetIntPref("Engine0", 0);
        PrefsHandler.Instance.SetIntPref("Tank0", 0);
        PrefsHandler.Instance.SetIntPref("Bumper0", 0);
        PrefsHandler.Instance.SetIntPref("Tires0", 0);
        PrefsHandler.Instance.SetIntPref("Gun0", 0);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void ContinueGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    //Tymczasowa metoda, zmiana na credits.
    public void ShowOptions()
    {
        Debug.Log("Pong");
    }
    //Zakończenia aplikacji.
    public void Exit()
    {
        Application.Quit();
    }
    #endregion
}