﻿//Kontrola kamery.
using UnityEngine;

public class CameraControll : MonoBehaviour
{
    #region Parameters
    public GameObject player;       //Obiekt gracza
    private Vector3 offset;         //Offset.
    #endregion
    #region Generic Methods
    // Obliczamy offset na podstawie pozycji kamery oraz gracza.
    void Start()
    {
        offset = transform.position - player.transform.position;
    }
    // Co klatkę zmieniamy pozycję kamery na podstawie pozycji gracza.
    void LateUpdate()
    {
        transform.position = player.transform.position + offset;
    }
    #endregion
}