﻿//Skrypt odpowiedzialny za gracza.
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    #region Parameters
    private int coins;              //Liczba monet.
    private float velocity;         //Przyspieszenie.
    private float maxSpeed;         //Maksymalna prędkość.
    private int maxFuel;            //Maksymalna liczba paliwa.
    private int maxHp;              //Maksymalne punkty życia.
    private int ammunition;         //Liczba amunicji.
    private int carIndex;           //Indeks pojazdu
    private Car carObject;          //Obiekt pojazdu
    private Cars cars;              //Lista samochodów
    public CarScript car;           //Obiekt kontroli pojazdu.
    #endregion
    #region Generic Methods
    // Przed rozpoczęciem rozgrywki przygotowujemy pojazd.
    void Awake()
    {
        GetParameters();
        SetCarParameters();
    }
    #endregion
    #region Non-Generic Methods
    //Metoda zwiększająca liczbę monet.
    public void CollectCoin()
    {
        coins += 10;
    }
    //Pobranie parametrów pojazdu oraz liczby monet z prefów.
    private void GetParameters()
    {
        coins = PrefsHandler.Instance.GetIntPref("Coins");
        carIndex = PrefsHandler.Instance.GetIntPref("Index");
        cars = ScriptableObject.CreateInstance<Cars>();
        carObject = cars.cars[carIndex];
        this.maxFuel = carObject.tankParameters[PrefsHandler.Instance.GetIntPref("Tank" + carIndex)];
        this.velocity = carObject.tiresParameters[PrefsHandler.Instance.GetIntPref("Tires" + carIndex)];
        this.maxSpeed = carObject.engineParameters[PrefsHandler.Instance.GetIntPref("Engine" + carIndex)];
        this.maxHp = carObject.bumperParameters[PrefsHandler.Instance.GetIntPref("Bumper" + carIndex)];
        this.ammunition = carObject.gunParameters[PrefsHandler.Instance.GetIntPref("Gun" + carIndex)];
        this.car.body.GetComponent<SpriteRenderer>().sprite = carObject.sprite;
    }
    //Ustawienie parametrów pojazdu.
    private void SetCarParameters()
    {
        car.SetParameters(maxSpeed, velocity, maxFuel, maxHp, ammunition);
    }
    #endregion
    #region Getters
    //Getter paliwa pojazdu.
    public int GetFuel()
    {
        return car.GetFuel();
    }
    //Getter monet.
    public int GetCoins()
    {
        return coins;
    }
    //Getter punktów życia pojazdu.
    public int GetHp()
    {
        return car.GetHp();
    }
    //Geter maksymalnego paliwa.
    public int GetMaxFuel()
    {
        return maxFuel;
    }
    //Getter maksymalnej liczby punktów życia.
    public int GetMaxHp()
    {
        return maxHp;
    }
    //Getter amunicji broni.
    public int GetAmmo()
    {
        return car.gun.GetAmmo();
    }
    #endregion
}