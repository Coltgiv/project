﻿//Skrypt odpowiedzialny za skrzynki.
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class CrateScript : MonoBehaviour
{
    #region Parameters
    private bool hitSomething = false;      //Czy obiekt został uderzony przez pojazd.
    public UnityEvent OnCollision;          //Event sygnalizujący kolizję.
    #endregion
    #region Generic Methods
    /*W przypadku kolziji, gdy zaszła ona z karoserią.
    * Jeżeli wcześniej skrzynka nie została uderzona wywołujemy event oraz zmieniamy wartość zmiennej hitSomething.
    * Rozpoczynamy enumerator CheckCollision()
    */
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Body")
        {
            if (!hitSomething)
            {
                OnCollision.Invoke();
                hitSomething = true;
            }
            StartCoroutine(CheckCollision());
        }
    }
    #endregion
    #region Non-Generic Methods
    //Enumerator który po 2 sekundach zmieni wartość zmiennej hitSomething na fałsz.
    IEnumerator CheckCollision()
    {
        yield return new WaitForSeconds(2);
        hitSomething = false;
    }
    #endregion
}