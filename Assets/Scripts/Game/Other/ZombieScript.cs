﻿//Skrypt odpowiedzialny za mechanikę zombie.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ZombieScript : MonoBehaviour
{
    #region Parameters
    private PolygonCollider2D polygonCollider;      //Collider obiektu.
    private Sprite sprite;                          //Sprite obiektu.
    public Animator animator;                       //Animator odpowiedzialny za zombie.
    private Rigidbody2D rb;                         //Rigidbody obiektu.
    public bool hitSomething = false;               //Czy wystąpiła kolizja z pojazdem.
    private bool isAlive = true;                    //Czy jest żywy.
    public UnityEvent OnCollision;                  //Event kolizji.
    #endregion
    #region Generic Methods
    // Na początku przypisujemy rigidbody.
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    /*  Na końcu sprawdzamy dystans do pojazdu i jeżeli jest w zasięgu to zmieniamy w animatorze zmienną. 
     *  Jeżeli obiekt był w zasiengu ale odległość przekracza 8 to niszczymy obiekt(zombie ominęło pojazd).
     */
    private void LateUpdate()
    {
        float distance = Vector2.Distance(FindObjectOfType<Camera>().transform.position, transform.position);
        if (distance < 6.5){
            animator.SetBool("IsInRange", true);
            rb.velocity = new Vector2(-2f, 0);
        } else if(distance > 8 && animator.GetBool("IsInRange")){
            Destroy(gameObject);
        }
        if(distance > 2 && animator.GetBool("CanAttack"))
        {
            animator.SetBool("CanAttack", false);
            animator.SetBool("DroveAway", true);
        }
        RefreshCollider();
    }
    //  W przypadku kolizji z karoserią zmieniamy animacje na atak oraz wywołujemy event i enumerator.
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Body")
        {
            animator.SetBool("CanAttack", true);
            animator.SetBool("DroveAway", false);
            if (!hitSomething && isAlive)
            {
                OnCollision.Invoke();
                hitSomething = true;
                StartCoroutine(RefreshAttack());
            }
        }
    }
    #endregion
    #region Non-Generic Methods
    /*  Metoda śmierci zombie.
     *  Wyłączamy kolizje z pojadzem, zmieniamy animacje oraz niszczymy obiekt po 1 sekundzie.
     */
    public void Die()
    {
        Physics2D.IgnoreCollision(FindObjectOfType<BodyScript>().GetComponent<PolygonCollider2D>(), polygonCollider);
        foreach (WheelScript wheel in FindObjectsOfType<WheelScript>())
        {
            Physics2D.IgnoreCollision(wheel.GetComponent<CircleCollider2D>(), polygonCollider);
        }
        isAlive = false;
        rb.velocity = new Vector2(0, 0);
        animator.SetBool("IsDead", true);
        Destroy(gameObject, 1);
    }
    //Metoda odświeżająca collider na podstawie spritu.
    private void RefreshCollider()
    {
        polygonCollider = GetComponent<PolygonCollider2D>();
        sprite = GetComponent<SpriteRenderer>().sprite;
        for (int i = 0; i < polygonCollider.pathCount; i++) polygonCollider.SetPath(i, null);
        polygonCollider.pathCount = sprite.GetPhysicsShapeCount();
        List<Vector2> path = new List<Vector2>();
        for (int i = 0; i < polygonCollider.pathCount; i++)
        {
            path.Clear();
            sprite.GetPhysicsShape(i, path);
            polygonCollider.SetPath(i, path.ToArray());
        }
    }
    //Za sekundę zmienia wartość hitSomething.
    IEnumerator RefreshAttack()
    {
        yield return new WaitForSeconds(1);
        hitSomething = false;
    }
    #endregion
}