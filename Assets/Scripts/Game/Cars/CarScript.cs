﻿//Skrypt odpowiedzialny za sam pojazd.
using UnityEngine;
using UnityEngine.Events;

public class CarScript : MonoBehaviour{
    #region Parameters
    [SerializeField]                    //[HideInInspector]
    private float maxSpeed;             //Maksymalna prędkość pojazdu

    private float velocity;             //Przyspieszenie pojazdu.
    private int fuel = 0;               //Wskaźnik paliwa pojazdu.
    private int maxFuel;                //Maksymalna ilość paliwa w pojeździe.
    private bool isOutOfFuel = false;   //Czy brakło paliwa.
    private int hp;                     //Liczba punktów życia.
    public BodyScript body;             //Obiekt karoseri pojazdu.
    public WheelScript frontWheel;      //Obiekt przedniego koła pojazdu.
    public WheelScript backWheel;       //Obiekt tylniego koła pojazdu.
    public GunScript gun;               //Obiekt broni pojazdu.
    public UnityEvent ColectCoin;       //Event zebrania monety.
    
    public UnityEvent Died;             //Event zginięcia.
    private bool canDestroy;            //Czy może niszczyć skrzynie/zombie
    #endregion
    #region Generic Methods
    // Na starcie przypisujemy ilość paliwa pojazdu.
    void Start()
    {
        fuel = maxFuel;
    }
    /*  Wywołujemy funkcje Drive() na początku.
     *  Przypisujemy do zmiennej nazwę obiektu z którym doszło do kolizji.
     *  W przypadku gdy doszło do kolizji z trasą oraz samochód jest na dachu, pojazd jest niszczony.
     *  Gdy doszło do kolizji z monetą wywołujemy event.
     *  W przypadku kolizji z skrzynią zmniejszamy prędkość.
     */
    private void FixedUpdate()
    {
        Drive();
        string nameOfCollison = body.GetNameOfCollisionObject();
        if (nameOfCollison != null)
        {
            if(nameOfCollison=="Track" && (body.GetIsFlipped()|| gun.GetIsFlipped()))
            {
                SelfDestory();
            }else if(nameOfCollison == "Coin")
            {
                ColectCoin.Invoke();
            }else if (nameOfCollison == "Crate" || nameOfCollison == "Zombie")
            {
                backWheel.SetSpeed(-backWheel.GetSpeed() * 0.3f);
                frontWheel.SetSpeed(-frontWheel.GetSpeed() * 0.3f);
            }
        }
    }
    #endregion
    #region Non-Generic Methods
    /*  Metoda odpowiedzialna za sterowanie pojadem.
     *  Odpowiednie działania w zależności od wciśniętego przycisku.
     *  Jeżeli prędkość jest odpowiednia można niszczyć obiekty
     */
    private void Drive()
    {
        if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A)) && !isOutOfFuel)
        {
            if (backWheel.GetSpeed() < maxSpeed)
            {
                backWheel.SetSpeed(velocity);
                frontWheel.SetSpeed(velocity);
            }
            fuel--;
        }
        else
        {
            backWheel.SlowDown();
            frontWheel.SlowDown();
        }
        if (Input.GetKey(KeyCode.W) && !isOutOfFuel)
        {
            body.RotateCar(2.5f);
        }
        else if (Input.GetKey(KeyCode.S) && !isOutOfFuel)
        {
            body.RotateCar(-2.5f);
        }
        if (fuel == 0.0 && !isOutOfFuel)
        {
            isOutOfFuel = true;
            SelfDestory();
        }
        if(frontWheel.GetSpeed() > 200 && backWheel.GetSpeed() > 200)
        {
            canDestroy = true;
        }
        else
        {
            canDestroy = false;
        }
        body.SetCanDestroyCrates(canDestroy);
    }
    //Niszczenie obiektu, wywoływany jest event, zatrzymanie kół.
    private void SelfDestory()
    {
        Died.Invoke();
        frontWheel.SetIsStopped(true);
        backWheel.SetIsStopped(true);
        Destroy(this);
    }
    //Metoda otrzymywania obrażeń, jeżeli zabrakło pkt życia obiekt jest niszczony.
    public void GetDamage()
    {
        hp--;
        if (hp == 0)
        {
            SelfDestory();
        }
    }
    //Metoda otrzymywania obrażeń od skrzynek, jeżeli prędkosć nie jest odpowiednia.
    public void GetDamageFromCrate()
    {
        if (!canDestroy)
        {
            hp--;
        }
        if (hp == 0)
        {
            SelfDestory();
        }
    }
    #endregion
    #region Setters
    //Ustawienie parametrów pojazdu.
    public void SetParameters(float maxSpeed, float velocity, int maxFuel, int hp, int ammunition)
    {
        this.maxFuel = maxFuel;
        this.maxSpeed = maxSpeed;
        this.velocity = velocity;
        this.hp = hp;
        this.gun.SetAmmo(ammunition);
    }
    #endregion
    #region Getters
    //Geter fuel.
    public int GetFuel()
    {
        return fuel;
    }
    //Geter hp.
    public int GetHp()
    {
        return hp;
    }
    #endregion
}