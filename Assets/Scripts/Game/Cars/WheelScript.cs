﻿//Skrypt odpowiedzialny za koło pojazdu.
using UnityEngine;

public class WheelScript : MonoBehaviour
{
    #region Parameters
    private WheelJoint2D wheel;     //Joint koła.
    private JointMotor2D motor;     //Silnik koła.
    private float speed = 0f;       //Prędkość koła.
    private bool isStopped = false; //Czy koła są zatrzymane.
    #endregion
    #region Generic Methods
    // Na początku przypisanie obiektu jointa oraz silnika.
    void Start()
    {
        wheel = GetComponent<WheelJoint2D>();
        motor = wheel.motor;
    }
    // Co kratkę wywoływanie metody obrotu koła,
    void Update()
    {
        RotateWheel();
    }
    #endregion
    
    #region Non-Generic Methods
    //Metoda odpowiedzialna za obrót koła.
    private void RotateWheel()
    {
        if (!isStopped)
        {
            float hforce = Input.GetAxis("Horizontal");
            motor.motorSpeed = speed * hforce;
            wheel.motor = motor;
        }
    }
    //Metoda zmniejszająca prędkość.
    public void SlowDown()
    {
        if (speed > 10)
        {
            speed -= 10;
        }
        else
        {
            speed = 0;
        }
    }
    #endregion
    #region Getters
    //Getter prędkości
    public float GetSpeed()
    {
        return this.speed;
    }
    #endregion
    #region Setters
    //Setter speed.
    public void SetSpeed(float speed)
    {
        this.speed += speed;
    }
    //Setter isStopped i zatrzymanie obrotu kół.
    public void SetIsStopped(bool isStopped)
    {
        this.isStopped = true;
        motor.motorSpeed = 0;
        wheel.motor = motor;
    }
    #endregion
}
