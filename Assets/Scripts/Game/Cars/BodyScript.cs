﻿//Skrypt odpowiedzialny za karoserię pojazdu.
using UnityEngine;

public class BodyScript : MonoBehaviour
{
    #region Parameters
    private Rigidbody2D rigidbody2D;        //Rigidbody karoserii.
    private string nameOfCollisonObject;    //Nazwa obiektu z którym doszło do kolizji.
    private bool isFlipped = false;         //Czy karosieria leżyna dachu.
    private float maxAngle = 20f;           //Maksymalny kąt obrotu, czyli podniesienia auta.
    private bool canDestroyCrates = false;  //Czy może niszczyć obiekty
    #endregion
    #region Generic Methods
    // Przypisanie rigidbody karoserii.
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();      
    }
    /** W przypadku kolizji.
     * collision to obiekt z którym doszło do kolizji.
     * Nazwę obiektu collision przypisujemy do zmiennej prywatnej,
     * w przypadku gdy do kolizji doszło z trasą, sprawdzamy czy karosieria jest obrócona.
     * Jeżeli doszło do kolizji z innym obiektem niż trasa i koła to niszczymy ten obiekt.
     * */
    private void OnCollisionEnter2D(Collision2D collision) 
    {
        nameOfCollisonObject = collision.gameObject.name;

        if (nameOfCollisonObject == "Track")
        {
            if (Vector3.Dot(transform.up, Vector3.down) > 0)
            {
                isFlipped = true;
            }
        }
        else if(nameOfCollisonObject=="Coin")
        {
            Destroy(collision.gameObject);
        }else if(nameOfCollisonObject =="Crate" && canDestroyCrates)
        {
            Destroy(collision.gameObject);
        }else if(nameOfCollisonObject == "Zombie" && canDestroyCrates)
        {
            collision.gameObject.GetComponent<ZombieScript>().Die();
        }
    }
    #endregion
    #region Non-Generic Methods
    /**
     * Obrócenie samochodu o podany kąt.
     */
    public void RotateCar(float angle)
    {
        if(rigidbody2D.rotation > - maxAngle && rigidbody2D.rotation < maxAngle)
            rigidbody2D.MoveRotation(rigidbody2D.rotation + angle);
    }
    #endregion
    #region Getters
    //Getter nameOfCollisonObject.
    public string GetNameOfCollisionObject()
    {
        string tmp = nameOfCollisonObject;
        nameOfCollisonObject = null;
        return tmp;
    }
    //Getter isFlipped.
    public bool GetIsFlipped()
    {
        return isFlipped;
    }
    //Getter canDestroy
    public bool GetCanDestroyCrates()
    {
        return this.canDestroyCrates;
    }
    #endregion
    #region Setters
    //Setter canDestroy
    public void SetCanDestroyCrates(bool canDestroyCrates)
    {
        this.canDestroyCrates = canDestroyCrates;
    }
    #endregion
}