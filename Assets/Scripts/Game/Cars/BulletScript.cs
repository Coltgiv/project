﻿//Skrypt odpowiedzialny z za pocisk.
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    #region Parameters
    private float velocity = 5f;        //Przyspieszczeni pocisku
    private Rigidbody2D rb;             //Rigidbody pocisku
    #endregion
    #region Generic Methods
    /*  Przypisanie rigidbody na starcie obiektu.
    *   Ustawienie przyspieszenia pocisku.
    *   Po 3 sekundach pocisk jest niszczony.
    */
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.velocity = transform.right * velocity;
        Destroy(gameObject, 3.0f);
    }
    /*  Wywoływane podczas kolizji z innym obiektem.
     *  collision to obiekt z którym doszło do kolizji.
     *  W przypadku gdy do kolizji doszło z skrzynią, skrzynia jest niszczona.
     *  W przypadku kolizji z Zombie, wywoływana jest metoda Die() obiektu zombie.
     *  Następnie pocisk jest niszczony.
     */
    private void OnCollisionEnter2D(Collision2D collision)
    {
       
        if(collision.gameObject.name == "Crate")
        {
            Destroy(collision.gameObject);
        }else if(collision.gameObject.name == "Zombie")
        {
           collision.gameObject.GetComponent<ZombieScript>().Die();
        }
        Destroy(gameObject);
    }
    #endregion
}