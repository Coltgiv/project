﻿//Skrypt odpowiedzialny za broń pojazdu.
using UnityEngine;

public class GunScript : MonoBehaviour
{
    #region Parameters
    public GameObject bullet;       //Obiekt pocisku.
    private Vector2 bulletPos;      //Pozycja pocisku.
    private int ammunition = 0;     //Liczba amunicji.
    private bool isFlipped = false; //Czy obrócony o 180 stopni.
    #endregion
    #region Generic Methods
    // Co klatkę sprawdzamy czy wciśnięta jest spacja, jeżeli tak to strzelamy.
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(ammunition > 0)
            {
                Fire();
                ammunition--;
            }
        }
    }
    //W przypadku kolizji z trasą ustawiwamy zmienną.
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Track")
        {
            isFlipped = true;
        }
    }
    #endregion
    #region Non-Generic Methods
    //Metoda odpowiedzialna za strzelanie. Oblicza pozycję pocisku a następnie kopije go z prefaba.
    private void Fire()
    {
        bulletPos = transform.position;
        bulletPos += new Vector2(0, 0);
        Instantiate(bullet, bulletPos, transform.rotation); //object pool
    }
    #endregion
    #region Setters
    //Setter ammunition.
    public void SetAmmo(int ammunition)
    {
        this.ammunition = ammunition;
    }
    #endregion
    #region Getters
    //Geter ammunition.
    public int GetAmmo()
    {
        return this.ammunition;
    }
    //Getter isFlipped.
    public bool GetIsFlipped()
    {
        return this.isFlipped;
    }
    #endregion
}